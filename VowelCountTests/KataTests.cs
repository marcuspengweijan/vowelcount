﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VowelCount.Tests
{
    [TestClass()]
    public class KataTests
    {
        [TestMethod()]
        public void GetVowelCountTest_5a()
        {
            Assert.AreEqual(5, Kata.GetVowelCount("abracadabra"));
        }
        [TestMethod()]
        public void GetVowelCountTest_Empty()
        {
            Assert.AreEqual(0, Kata.GetVowelCount(""));
        }
        [TestMethod()]
        public void GetVowelCountTest_One()
        {
            Assert.AreEqual(1, Kata.GetVowelCount("A"));
            Assert.AreEqual(1, Kata.GetVowelCount("e"));
            Assert.AreEqual(1, Kata.GetVowelCount("I"));
            Assert.AreEqual(1, Kata.GetVowelCount("o"));
            Assert.AreEqual(1, Kata.GetVowelCount("U"));
        }
        [TestMethod()]
        public void GetVowelCountTest_None()
        {
            Assert.AreEqual(0, Kata.GetVowelCount("bbbcccddd"));
        }
    }
}