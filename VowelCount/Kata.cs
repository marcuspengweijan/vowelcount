﻿using System.Linq;

namespace VowelCount
{
    public class Kata
    {
        private const string Vowels = "aeiou";
        public static int GetVowelCount(string str)
        {
            return str.ToLower().Count(x => Vowels.Contains(x));
        }
    }
}
